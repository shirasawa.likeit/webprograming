<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/control.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->

</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row"
			style="background-color: #019fe6;">
			<ul class="navbar-nav navbar-dark flex-row mr-auto"
				style="background-color: #019fe6;">
				<li class="nav-item active"><a class="nav-link"
					href="LoginServlet">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item">${userInfo.name}さん</li>
				<li class="nav-item"><a class="btn btn-outline-light"
					href="index.html">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="text-center mb-4">
		<h1 class="h3 mb-3 font-weight-normal">ユーザー情報削除</h1>
	</div>
	<!-- body -->
	<div class="container">
		<div class="delete-area">
			<p>ログインID:${loginId}を消去しますか？</p>
			<form method="post" class="form-signin" name="form_1" action="UserDaleteServlet">
				<div class="row">

					<div class="col-sm-6">
						<a href="UserListServlet" class="btn btn-light btn-block">いいえ</a>
					</div>
					<div class="col-sm-6">
						<a href="javascript:form_1.submit()"
							class="btn btn-primary btn-block">はい</a> <input type="hidden"
							name="id" class="form-control" id="id" value=${id} >

					</div>
				</div>
			</form>
		</div>
	</div>
</body>

</html>