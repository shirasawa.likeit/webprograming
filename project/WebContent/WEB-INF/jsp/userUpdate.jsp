<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/control.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->
</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row"
			style="background-color: #019fe6;">
			<ul class="navbar-nav navbar-dark flex-row mr-auto"
				style="background-color: #019fe6;">
				<li class="nav-item active"><a class="nav-link"
					href="LoginServlet">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class=" nav-item"><a class="nav-link" href="#">ログインユーザの名前を表示</a>
				</li>
				<li class="nav-item"><a class="btn btn-outline-light"
					href="index.html">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->

	<div class="container">
		<form method="post" class="form-signin" action="UserUpdateServlet">
			<div class="text-center mb-4">
				<h1 class="h3 mb-3 font-weight-normal">ユーザー情報更新</h1>
			</div>

			<br>
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
			<br>

			<input type="hidden" name="id" class="form-control" id="id"
				value=${id} >

			<div class="form-group row">
				<label for="inputloginId">ログインID</label>
				<p class="form-control-plaintext">${loginId}</p>
				<input type="hidden" name="loginId" class="form-control"
					id="loginId" value=${loginId} >
			</div>
			<br>
			<div class="form-group row">
				<label for="inputpass">パスワード</label> <input type="text"
					id="ilogin-pass" name="login-pass" class="form-control">
			</div>
			<br>
			<div class="form-group row">
				<label for="inputpass">パスワード(確認)</label> <input type="text"
					id="login-pass-sc" name="login-pass-sc" class="form-control">
			</div>
			<br>
			<div class="form-group row">
				<label for="inputpass">ユーザー名</label> <input type="text" id="name"
					name="name" class="form-control" value=${name}>
			</div>
			<br>
			<div class="form-group row">
				<label for="continent" class="control-label">生年月日</label> <input
					type="date" name="birth-day" id="birth-day" class="form-control"
					size="30" value=${birthday}>
			</div>
			<br>
			<button class="btn btn-primary">更新</button>
		</form>
	</div>
</body>
</html>