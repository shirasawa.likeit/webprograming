<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/control.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->

</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row"
			style="background-color: #019fe6;">
			<ul class="navbar-nav navbar-dark flex-row mr-auto"
				style="background-color: #019fe6;">
				<li class="nav-item active"><a class="nav-link"
					href="LoginServlet">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class=" nav-item"><a class="nav-link" href="#">ログインユーザの名前を表示</a>
				</li>
				<li class="nav-item"><a class="btn btn-outline-light"
					href="index.html">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container">


		<div class="container">
			<div class="text-center mb-4">
				<h1 class="h3 mb-3 font-weight-normal">ユーザー情報更新</h1>
			</div>

			<form method="post" action="userList.html" class="form-horizontal">
				<div class="form-group row">
					<label for="loginId" class="col-sm-6 col-form-label text-right">ログインID</label>
					<div class="col-sm-4">
						<p class="form-control-plaintext">${loginId}</p>
					</div>
				</div>

				<div class="form-group row">
					<label for="userName" class="col-sm-6 col-form-label text-right">ユーザ名</label>
					<div class="col-sm-4">
						<p class="form-control-plaintext">${name}</p>
					</div>
				</div>

				<div class="form-group row">
					<label for="birthDate" class="col-sm-6 col-form-label text-right">生年月日</label>
					<div class="col-sm-4">
						<p class="form-control-plaintext">${birthday}</p>
					</div>
				</div>

				<div class="form-group row">
					<label for="createDate" class="col-sm-6 col-form-label text-right">新規登録日時</label>
					<div class="col-sm-5">
						<p class="form-control-plaintext">${createDate}</p>
					</div>
				</div>

				<div class="form-group row">
					<label for="updateDate" class="col-sm-6 col-form-label text-right">更新日時</label>
					<div class="col-sm-5">
						<p class="form-control-plaintext">${updateDate}</p>
					</div>
				</div>

				<div class="col-xs-4">
					<a href="userListServlet">戻る</a>
				</div>

			</form>


		</div>
	</div>
</body>
</html>