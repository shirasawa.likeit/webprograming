<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/control.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->
</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row"
			style="background-color: #019fe6;">
			<ul class="navbar-nav navbar-dark flex-row mr-auto"
				style="background-color: #019fe6;">
				<li class="nav-item active"><a class="nav-link"
					href="LoginServlet">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item">${userInfo.name} さん</li>
				<li class="nav-item">
				<a class="btn btn-outline-light"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container">

		<!-- 新規登録ボタン -->
		<div class="create-button-area">
			<a class="btn btn-primary btn-lg" href="UserAddServlet">新規登録</a>
		</div>

		<div class="text-center mb-4">
			<h1 class="h3 mb-3 font-weight-normal">ユーザー一覧</h1>
		</div>
		<br>

		<!-- 検索ボックス -->
		<div class="search-form-area">
			<div class="panel-body">
				<form method="post" action="#" class="form-horizontal">
					<div class="form-group row">
						<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="login-Id" name="login-Id">
						</div>
					</div>

					<div class="form-group row">
						<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="user-Name" name="user-Name">
						</div>
					</div>

					<div class="form-group row">

						<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>

						<div class="row col-sm-10">
							<div class="col-sm-5">
								<input type="date" name="date-start" id="date-start"
									class="form-control" />
							</div>

							<div class="col-sm-1 text-center">~</div>
							<div class="col-sm-5">
								<input type="date" name="date-end" id="date-end"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="text-right">
						<button type="submit" value="検索"
							class="btn btn-primary form-submit">検索</button>
					</div>
				</form>
			</div>
		</div>

		<!-- 検索結果一覧 -->
		<div class="table-responsive">
			<table class="table table-striped">
				<thead class="thead" style="background-color: #019fe6;">
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="user" items="${userList}" >
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td><a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
						 <c:if test="${userInfo.loginId == 'admin' or user.loginId == userInfo.loginId}" >
							<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
						</c:if>

						<c:if test="${userInfo.loginId == 'admin'}" >
							<a class="btn btn-danger"  href="UserDaleteServlet?id=${user.id}">削除</a>
							</c:if>
							</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

	</div>

</body>

</html>