<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/login.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->

</head>
<body>
	<form class="form-signin" action="LoginServlet" method="post">
		<div class="text-center mb-4">
			<h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1>
		</div>

		<br>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<br>

		<div class="form-group">
			<label for="inputloginId">ログインID</label> <input name="loginId"
				id="inputLoginId" class="form-control" placeholder="ログインIDを入力してください"
				required autofocus>
		</div>
		<br>
		<div class="form-group">
			<label for="inputpass">パスワード</label> <input type="password"
				name="password" id="inputPassword" class="form-control"
				placeholder="パスワードを入力してください" required>
		</div>
		<br>
		<button class="btn btn-outline-primary" type="submit">ログイン</button>
	</form>
</body>
</html>