package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Pasword;
import dao.UserDao;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserAddServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 新規登録のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//リクエストパラメータの文字コードを指定(文字列取得のため)
		request.setCharacterEncoding("UTF-8");

		UserDao dao = new UserDao();

		//入力されている情報取得
		String loginId = request.getParameter("inputloginId");
		String password = request.getParameter("login-pass");
		String password_sc = request.getParameter("login-pass-sc");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth-day");

		if (loginId.isEmpty() || password.isEmpty() || password_sc.isEmpty() || name.isEmpty()
				|| birth_date.isEmpty()) {
			//空欄があった場合
			//エラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			request.setAttribute("inputloginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthday", birth_date);

			//新規登録画面へ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
			return;

		} else if (!(password.equals(password_sc))) {
			//パスワードが違っていた場合
			//エラーメッセージと新規登録画面に表示する情報をセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("inputloginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthday", birth_date);

			//新規登録画面へ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
			return;

		} else if (dao.findLoginId(loginId) != null) {
			//ログインIDがすでに登録されているものだった場合
			//エラーメッセージと新規登録画面に表示する情報をセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("inputloginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthday", birth_date);

			//新規登録画面へ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
			return;


		} else {
			//新規登録成功時
			//作成、更新日時の取得
			Calendar cl = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");

			//パスワードを暗号化
			String passWord = Pasword.password(password);

			int id = dao.newID();

			//登録処理
			dao.insert(loginId, name, birth_date, passWord, sdf.format(cl.getTime()),
					sdf.format(cl.getTime()));
			response.sendRedirect("UserListServlet");
		}
	}

}
