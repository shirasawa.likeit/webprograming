package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDaleteServlet
 */
@WebServlet("/UserDaleteServlet")
public class UserDaleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDaleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ID取得
		String id = request.getParameter("id");

		UserDao dao = new UserDao();
		//IDに紐づく情報取得
		User user = dao.user_id(id);

		request.setAttribute("loginId", user.getLoginId());
		request.setAttribute("id", id);

		//削除画面へ
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ID取得
		String id = request.getParameter("id");

		UserDao dao = new UserDao();
		//削除処理
		dao.delete(id);
		//ユーザー一覧へ
		response.sendRedirect("UserListServlet");
		return;

	}

}
