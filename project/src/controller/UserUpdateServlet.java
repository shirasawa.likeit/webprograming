package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Pasword;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// IDを取得
		String id = request.getParameter("id");

		UserDao dao = new UserDao();

		//取得したIDに紐づく情報を取得
		User user = dao.user_id(id);

		//更新画面に表示する情報
		request.setAttribute("id", id);
		request.setAttribute("loginId", user.getLoginId());
		request.setAttribute("name", user.getName());
		request.setAttribute("birthday", user.getBirthDate());
		request.setAttribute("createDate", user.getCreateDate());
		request.setAttribute("updateDate", user.getUpdateDate());

		//更新画面へ
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定(文字列取得のため)
		request.setCharacterEncoding("UTF-8");

		//更新画面に入力された情報を取得
		String id = request.getParameter("id");
		String loginid = request.getParameter("loginId");
		String pass = request.getParameter("login-pass");
		String pass_sc = request.getParameter("login-pass-sc");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birth-day");

		UserDao dao = new UserDao();

		//更新日時の取得
		Calendar cl = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");

		if (name.isEmpty() || birthday.isEmpty()) {
			//ユーザー名、生年月日が空欄時
			//エラーメッセージと更新画面へ表示する情報をセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("id", id);
			request.setAttribute("loginId", loginid);
			request.setAttribute("name", name);
			request.setAttribute("birthday", birthday);

			//更新画面へ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		} else {

			if (pass.isEmpty() || pass_sc.isEmpty()) {
				//pass更新なしの場合
				dao.update(id, name, birthday, sdf.format(cl.getTime()));
			} else {
				if (pass.equals(pass_sc)) {
					//pass更新ありの場合
					//パスワードを暗号化
					String passWord = Pasword.password(pass);
					//更新処理
					dao.update_pass(id, name, passWord, birthday, sdf.format(cl.getTime()));
				} else {
					//passwordが間違っている場合
					//エラーメッセージと更新画面へ表示する情報をセット
					request.setAttribute("errMsg", "入力された内容は正しくありません");
					request.setAttribute("id", id);
					request.setAttribute("loginId", loginid);
					request.setAttribute("name", name);
					request.setAttribute("birthday", birthday);

					//更新画面へ
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
					dispatcher.forward(request, response);
					return;
				}
			}

		}
		//成功時：更新画面へ
		response.sendRedirect("UserListServlet");

	}

}
