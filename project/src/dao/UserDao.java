package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return ユーザー情報
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// レコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");

			//戻す情報をUserに追加
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return 全ユーザー情報一覧
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SQL(管理者以外、出力)
			String sql = "SELECT * FROM user WHERE NOT name = '管理者'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ユーザー一覧：DB検索
	 * @param loginId
	 * @param name
	 * @param date_start
	 * @param date_end
	 * @return 検索条件に合うユーザー一覧
	 */
	public List<User> userserch(String loginId, String name, String date_start, String date_end) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE NOT name='管理者'";

			//loginIdがあったらsqlに追加
			if (!(loginId.equals(""))) {
				sql += " AND login_id = '" + loginId + "'";
			}
			//nameがあったらsqlに追加
			if (!(name.equals(""))) {
				sql += " AND name LIKE '%" + name + "%'";
			}
			//検索日付範囲
			if (!(date_start.equals(""))) {
				sql += " AND birth_date >=" + date_start + "'";
			}
			if (!(date_end.equals(""))) {
				sql += " AND birth_date <=" + date_end + "'";
			}
			//SQL確認
			System.out.println(sql);

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String Name = rs.getString("name");
				Date birthdate = rs.getDate("birth_date");
				String pass = rs.getString("password");
				String createdate = rs.getString("create_date");
				String updatedate = rs.getString("update_date");
				User user = new User(id, loginid, Name, birthdate, pass, createdate, updatedate);

				userList.add(user);
			}

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ユーザー新規登録：DB登録処理
	 * @param id
	 * @param loginId
	 * @param name
	 * @param birthday
	 * @param pass
	 * @param createdate
	 * @param updatedate
	 * @return なし
	 */
	public void insert(String loginId, String name, String birthday, String pass, String createdate,
			String updatedate) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user VALUES(?,?,?,?,?,?)";

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, birthday);
			stmt.setString(4, pass);
			stmt.setString(5, createdate);
			stmt.setString(6, updatedate);

			//SQL実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}

	/**
	 * ユーザー新規登録時ID自動採番
	 * @return 登録するID
	 */
	public int newID() {
		Connection conn = null;
		int count = 0;
		try {
			//データベース接続
			conn = DBManager.getConnection();

			String sql = "SELECT MAX(id) FROM user";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
				count = rs.getInt("id");
				System.out.println(count);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		count++;
		return count;
	}

	/**
	 * ユーザー更新：初期表示
	 * @param id
	 * @return 更新するユーザー情報
	 */
	public User user_id(String id) {
		Connection conn = null;
		User user = null;

		try {

			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = '" + id + "'";
			System.out.println(sql);

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String loginid = rs.getString("login_id");
				String Name = rs.getString("name");
				Date birthdate = rs.getDate("birth_date");
				String createdate = rs.getString("create_date");
				String updatedate = rs.getString("update_date");

				user = new User(loginid, Name, birthdate, createdate, updatedate);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;

	}

	/**
	 * ユーザー更新：pass更新なし
	 * @param id
	 * @param name
	 * @param birthday
	 * @param updatedate
	 * @return なし
	 */
	public void update(String id, String name, String birthday, String updatedate) {
		Connection conn = null;
		PreparedStatement stmt = null;
		System.out.println("passなし");
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name=?, birth_date=?, update_date=? WHERE id = ?";

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, birthday);
			stmt.setString(3, updatedate);
			stmt.setString(4, id);

			//SQL実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}
	/**
	 * ユーザー更新：pass更新あり
	 * @param id
	 * @param name
	 * @param birthday
	 * @param updatedate
	 * @return なし
	 */
	public void update_pass(String id, String name, String pass, String birthday, String updatedate) {
		Connection conn = null;
		PreparedStatement stmt = null;
		System.out.println("passあり");
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name=?, password=?, birth_date=?, update_date=? WHERE id = ?";

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, pass);
			stmt.setString(3, birthday);
			stmt.setString(4, updatedate);
			stmt.setString(5, id);

			//SQL実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}

	/**
	 * ユーザー削除
	 * @param id
	 * @return なし
	 */
	public void delete(String id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            String sql = "DELETE FROM user WHERE id =?;";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);
            pStmt.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

	}

	public User findLoginId(String loginId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE login_id = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();


            if (!rs.next()) {
                return null;
            }

            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

}