package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Pasword {
	public static String password(String password) {

		//ハッシュ生成前にバイト配列に書き換える際のCharaset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";
		String Pass="";
		try {
			//ハッシュ生成処置
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			Pass = DatatypeConverter.printHexBinary(bytes);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		//MD5変換後のパスワード出力
		return Pass;

	}
}
